<?php    

get_header();


?>
      
     

  <?php while(have_posts()) {
         the_post();
        
    ?>

     <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="post-preview">
          <a href="<?php the_permalink();?>">
            <h2 class="post-title">
              <?php  the_title();?>
            </h2>
            <h3 class="post-subtitle">
              <?php wp_trim_words(get_the_content(),12); ?>
            </h3>
          </a>
          <p class="post-meta">Posted by
            <a href="#">Start Bootstrap</a>
            on September 24, 2019</p>
        </div>
        <hr>
      </div>
    </div>
  </div>
  <?php  }wp_reset_postdata();?>

  <!-- Pager -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="clearfix">
          <a class="btn btn-primary float-right"   href="#">Mas vacantes &rarr;</a>
        </div>
      </div>
    </div>
  </div>

  
  

  
<?php
get_footer(); 

?>