<?php


function load_stylesheets(){


    //wp_enqueue_script('main-university-js',get_theme_file_uri('/js/scripts-bundled.js'),NULL,microtime(),true);    
    wp_register_style('bootstrap_stylesheet',get_stylesheet_directory_uri() . '/vendor/bootstrap/css/bootstrap.min.css',array(),false,"all");
    wp_register_style('fontawasome',get_stylesheet_directory_uri() . '/vendor/fontawesome-free/css/all.min.css',array(),false,"all");
    wp_register_style('template_stylesheet',get_stylesheet_directory_uri() . '/css/clean-blog.css',array(),false,"all");

    wp_enqueue_style('style', get_stylesheet_uri(), NULL , microtime() );
    wp_enqueue_style('bootstrap_stylesheet');
    wp_enqueue_style('fontawasome');
    wp_enqueue_style('template_stylesheet');
    wp_enqueue_style('https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic','https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800');
}

add_action("wp_enqueue_scripts", "load_stylesheets");



?>