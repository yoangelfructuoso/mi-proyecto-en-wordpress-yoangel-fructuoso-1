<!DOCTYPE html>
<html lang="en">


<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>EmpleosJobs </title>

   <?php wp_head() ?>

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top is-fixed is-visible" id="mainNav">
    <div class="container">
      <a class="navbar-brand" style="color: #42A8D5 "  href="index.php">EmpleosJobs</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" style="color: #42A8D5 ;" href="">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" style="color: #42A8D5 ;" href="<?php echo site_url('?page_id=37') ?>">Categorias</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" style="color: #42A8D5 ;"href="<?php echo site_url('?page_id=19') ?>">Contactos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link"  style="color: #42A8D5 ;" href="?page_id=20">Sobre nosotros</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('https://images8.alphacoders.com/368/thumb-1920-368904.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto  ">
          <div class="site-heading" style="color: #D7E5E6">
            <h1>EmpleosJobs</h1>
            <span class="subheading" style="color: white ">Aqui encontraras nuevas oportunidades

            </span>
          </div>
        </div>
      </div>
    </div>
  </header>
    
</body>

</html>